LIGO Channel Lists
==================

This repository contains files describing lists of interesting data channels for use in LIGO Detector Characterization

All changes should be made using [pull/merge requests](https://help.github.com/articles/using-pull-requests/) via the github.com [fork-and-pull model](https://guides.github.com/introduction/flow/).

File structure
--------------

Each observing epoch has a directory containing its channel lists. Within there are files matching the following naming convention

```
{ifo}-{epoch}-{level}.ini
```

Where the levels are as follows

- 'standard': representative channels from all systems under normal conditions
- 'reduced': a sub-set of 'standard' with a reduced list for PEM, SEI, and SUS systems
- 'deep': a over-complete super-set of 'standard' with non-archived channels

The 'standard' and 'reduced' levels contains only those channels that are archived in the `{ifo}_R` channel lists.
