#!/usr/bin/env python

"""Convert a LIGO Channel List INI file into an Omicron channels.list file
"""

from __future__ import (print_function, division)

import argparse
import re
import fnmatch
from math import (ceil, log)

try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser

from numpy import isinf

from gwpy.detector import ChannelList
from gwpy.utils.compat import OrderedDict

NAMES = [
    ['STD', 1024, 0],
    ['LOW', 0, 0],
]


def omicron_segment_parameters(flow, fhigh):
    """Calculate segment parameters for Omicron
    """
    # get segment duration
    if fhigh >= 4096:
        segment = 64
    elif fhigh >= 512:
        segment = 128
    elif fhigh >= 128:
        segment = 256
    else:
        segment = 512
    # get overlap
    if flow < 1:
        overlap = max(128, segment//16.)
    else:
        overlap = segment // 16
    # get chunk duration
    nseg = 2
    chunk = (segment-overlap) * nseg + overlap
    # get sample frequency (round 2*fhigh up to nearest power of 2)
    sampling = 2 ** int(ceil(log(fhigh * 2, 2)))
    return sampling, chunk, segment, overlap


parser = argparse.ArgumentParser()
parser.add_argument('clf', help='path of channel list file')
parser.add_argument('-o', '--output-file', default='omicron-channels.ini',
                    help='path of output Omicron channels file')
parser.add_argument('--pem', action='append',
                    default=['*PEM-*', '*ISI-GND_*'],
                    help='list of glob-style regex patterns to match channels '
                         'that should run all the time, default: %(default)s')
parser.add_argument('--priority', action='append',
                    default=['*GDS-CALIB_STRAIN'],
                    help='list of glob-style regex patterns to match priotiy '
                         'channels, default: %(default)s')

args = parser.parse_args()

re_priority = re.compile('(%s)' % '|'.join(
    map(fnmatch.translate, args.priority)))
re_pem = re.compile('(%s)' % ('|'.join(
    map(fnmatch.translate, args.pem))))

# read channel list file
channels = ChannelList.read(args.clf)

# build groups
groups = OrderedDict()
for channel in channels:
    sf = channel.sample_rate.value
    fr = tuple(channel.frequency_range.value)
    try:
        qh = channel.params['qhigh']
    except KeyError:
        qr = None
    else:
        qr = (3.3166, qh)
    if isinf(fr[1]):
        fr = (fr[0], channel.sample_rate.value / 2.)
    if re_priority.search(channel.name):
        state = '%s:DMT-CALIBRATED:1' % channel.ifo
    elif re_pem.search(channel.name):
        state = None
    else:
        state = '%s:DMT-GRD_ISC_LOCK_NOMINAL:1' % channel.ifo
    key = (channel.frametype or '%s_R' % channel.ifo, fr, qr, state)
    try:
        groups[key].append(channel.name)
    except KeyError:
        groups[key] = [channel.name]

# write omicron channels.list file
outclf = ConfigParser(dict_type=OrderedDict)
pemx = 1
stdx = 1
lowx = 1
for i, key in enumerate(groups):
    ft, fr, qr, state = key
    if state is not None and state.endswith('DMT-CALIBRATED:1'):
        name = 'GW'
    elif state is None:
        name = 'PEM%d' % pemx
        pemx += 1
    elif fr[1] >= 256:
        name = 'STD%d' % stdx
        stdx += 1
    else:
        name = 'LOW%s' % lowx
        lowx += 1
    if not outclf.has_section(name):
        outclf.add_section(name)
    if qr is not None:
        outclf.set(name, 'q-range', ' '.join(map(str, qr)))
    outclf.set(name, 'frequency-range', ' '.join(map(str, fr)))
    outclf.set(name, 'frametype', ft)
    if state:
        outclf.set(name, 'state-flag', state)
    sampling, chunk, segment, overlap = omicron_segment_parameters(*fr)
    outclf.set(name, 'sample-frequency', sampling)
    outclf.set(name, 'chunk-duration', chunk)
    outclf.set(name, 'segment-duration', segment)
    outclf.set(name, 'overlap-duration', overlap)
    if name == 'GW':
        outclf.set(name, 'mismatch-max', 0.2)
        outclf.set(name, 'snr-threshold', 5)
    else:
        outclf.set(name, 'mismatch-max', 0.35)
        outclf.set(name, 'snr-threshold', 5.5)

    outclf.set(name, 'channels', '\n'.join(groups[key]))
    print(name)

with open(args.output_file, 'w') as f:
    outclf.write(f)
