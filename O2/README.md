Observing Run 2
===============

This directory contains those channel lists for the second aLIGO observing run, 
beginning in summer 2016.

O2 Omicron Online Documentation
===============

h(t)
----
**Summary** 

Triggers produced for GDS-CALIB_STRAIN have a wide frequency range, up to 8 kHz, 
a finely populated sine-Gaussian template basis with 0.2 allowed mismatch, 
and an lower SNR threshold 
of 5. Triggers are only produced when the ifo is running nominally and 
calibration is also in a good state. 

**Parameters**

- Frequency range: 4 - 8192 Hz
- Q range: 3.3155 - 150
- State required to produce triggers: DMT-CALIBRATED:1
- Chunk duration: 124
- Segment duration: 64
- Overlap duration: 4
- Maximum allowed mismatch: 0.2
- SNR threshold: 5

---

Auxiliary channels
---

Auxiliary channels are processed with a less finely space sine-Gaussian tiling
(a maximum allowed mismatch of 0.35) and an SNR threshold of 5.5

Auxiliary channels are grouped by valid frequency range, sampling rate, and 
required state for producing triggers. Omicron parameters for each group are 
designed to maximize the amount of information from auxiliary channel triggers, 
as defined here: https://git.ligo.org/detchar/ligo-channel-lists/blob/master/tools/clf-to-omicron.py.

Omicron triggers are produced for most auxiliary channels with the ifo is 
operating in a nominal state (DMT-GRD_ISC_LOCK_NOMINAL:1) except for ground 
motion monitors and PEM sensors, which always produce triggers. 
